//---------------------------------------------------------------------------

#pragma once

#ifndef FLOW_TIMER_H
#define FLOW_TIMER_H

//---------------------------------------------------------------------------

#include <function/function.h>
#include <boost/bind.hpp>
#include <boost/asio/steady_timer.hpp>
#include <boost/asio/high_resolution_timer.hpp>
#include <boost/asio/placeholders.hpp>

#include "context.h"

//---------------------------------------------------------------------------

namespace asd
{
    namespace flow
    {
        using namespace std::chrono;
        using namespace std::chrono_literals;

        using clock = std::chrono::high_resolution_clock;
        using time_marker = time_point<clock>;
        using ticks_t = long long;

        class default_timer_provider
        {
        public:
            using result_type = void;
    
            template <class F, class ... A>
            using return_type = decltype(declval<F>()(declval<A>()...));

            template <class F>
            default_timer_provider(F callback) : _callback(callback) {}
            
        protected:
            void call() {
                _callback();
            }

            function<void()> _callback;
        };

        template <class Impl, class Provider = default_timer_provider>
        class generic_timer : public Provider, public flow::object
        {
        public:
            template <class Dur, class F>
            generic_timer(flow::context & flow, Dur duration, F callback) : Provider(callback), _duration(duration_cast<nanoseconds>(duration)), _impl(flow.io(), _duration) {}

            template <class F>
            void bind(F callback) {
                this->_callback = callback;
            }

            void start() {
                if (_active) {
                    stop();
                }

                _active = true;
                next();
            }

            template <class Dur, class F, typename = typename Provider::template return_type<F>>
            static generic_timer & start(flow::context & flow, Dur duration, F callback) {
                auto & timer = flow.add<generic_timer>(duration, callback);
                timer.start();
                return timer;
            }

            template <class Dur, class F, class ... A, useif<(sizeof...(A) > 0)>, typename = typename Provider::template return_type<F, A...>>
            static generic_timer & start(flow::context & flow, Dur duration, F callback, A && ... args) {
                auto & timer = flow.add<generic_timer>(duration, std::bind(callback, std::forward<A>(args)...));
                timer.start();
                return timer;
            }

            void stop() {
                if (!_active) {
                    return;
                }

                _active = false;
                _impl.cancel();
            }

        private:
            void update(const boost::system::error_code & /* e */) {
                this->call();

                if (_active) {
                    _impl.expires_at(_impl.expiry() + _duration);
                    next();
                }
            }

            void next() {
                _impl.async_wait(boost::bind(&generic_timer::update, this, boost::asio::placeholders::error));
            }

            nanoseconds _duration;
            boost::asio::steady_timer _impl;
            bool _active = false;
        };

        class tick_timer_provider
        {
        public:
            using result_type = ticks_t;
    
            template <class F, class ... A>
            using return_type = decltype(declval<F>()(declval<result_type>(), declval<A>()...));

            template <class F>
            tick_timer_provider(F callback) : _callback(callback) {}

        protected:
            void call() {
                _callback(_ticks++);
            }

            function<void(result_type)> _callback;
            result_type _ticks = 0;
        };
    
        template <class Duration>
        class period_timer_provider
        {
        public:
            using result_type = Duration;
            using clock = std::chrono::steady_clock;
            using time_point = typename clock::time_point;
        
            template <class F, class ... A>
            using return_type = decltype(declval<F>()(declval<result_type>(), declval<A>()...));
    
            template <class F>
            period_timer_provider(F callback) : _callback(callback) {}
    
        protected:
            void call() {
                auto current_time = clock::now();
                _callback(std::chrono::duration_cast<result_type>(current_time - _last_time));
                _last_time = current_time;
            }
        
            function<void(result_type)> _callback;
            time_point _last_time = clock::now();
        };

        using timer = generic_timer<boost::asio::steady_timer>;
        using tick_timer = generic_timer<boost::asio::steady_timer, tick_timer_provider>;
        
        template <class Duration>
        using period_timer = generic_timer<boost::asio::steady_timer, period_timer_provider<Duration>>;
    }
}

//---------------------------------------------------------------------------
#endif
