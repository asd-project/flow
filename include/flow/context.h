//---------------------------------------------------------------------------

#pragma once

#ifndef FLOW_CONTEXT_H
#define FLOW_CONTEXT_H

//---------------------------------------------------------------------------

#include <boost/asio/io_context.hpp>
#include <thread>
#include <chrono>
#include <container/array_list.h>

//---------------------------------------------------------------------------

namespace asd
{
    namespace flow
    {
        using namespace std::chrono_literals;

        class object
        {
        public:
            virtual ~object() {}
        };

        class context
        {
        public:
            boost::asio::io_context & io() {
                return _io;
            }

            void run() {
                _io.run();
            }

            void stop() {
                _io.stop();
            }

            template <class T, class ... A, useif<is_base_of<flow::object, T>::value, is_constructible<T, flow::context &, A...>::value>>
            T & add(A && ... args) {
                return static_cast<T &>(**_objects.emplace(_objects.end(), std::make_unique<T>(*this, std::forward<A>(args)...)));
            }

            template <class T, useif<is_base_of<flow::object, T>::value>>
            T & add(std::unique_ptr<T> && o) {
                return static_cast<T &>(**_objects.emplace(_objects.end(), std::forward<std::unique_ptr<object>>(o)));
            }

            template <class T, useif<is_base_of<flow::object, T>::value>>
            context & operator << (std::unique_ptr<T> && o) {
                add(std::forward<std::unique_ptr<T>>(o));
                return *this;
            }

        private:
            boost::asio::io_context _io;
            array_list<std::unique_ptr<object>> _objects;
        };
    }
}

//---------------------------------------------------------------------------
#endif
