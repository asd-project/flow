import os

from conans import ConanFile, CMake

project_name = "flow"


class FlowConan(ConanFile):
    name = "asd.%s" % project_name
    version = "0.0.1"
    license = "MIT"
    author = "bright-composite"
    url = "https://gitlab.com/asd-project/%s" % project_name
    description = "Library for time flow control"
    topics = ("asd", project_name)
    generators = "cmake"
    exports_sources = "include*", "CMakeLists.txt", "bootstrap.cmake"
    requires = (
        "asd.core/0.0.1@bright-composite/testing",
        "boost_asio/1.69.0@bincrafters/stable"
    )
    
    def source(self):
        pass
    
    def build(self):
        cmake = CMake(self)
        cmake.configure()
        cmake.build()
    
    def package(self):
        self.copy("*.h", dst="include", src="include")
