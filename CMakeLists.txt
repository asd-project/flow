#--------------------------------------------------------
#	module flow
#--------------------------------------------------------

cmake_minimum_required(VERSION 3.3)

project(flow VERSION 0.1)

#--------------------------------------------------------

include(bootstrap.cmake)

#--------------------------------------------------------

module(INLINE)
    domain(flow)

    group(include Headers)
    files(
        context.h
        timer.h
    )
endmodule()

#--------------------------------------------------------
